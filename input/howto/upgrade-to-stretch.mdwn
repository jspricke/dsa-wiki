# Upgrade from jessie to stretch:

Make sure to coordinate with whoever uses the host, be it the buildd
operator, the QA team, the ftp team or just announce it on IRC if it's a
general developer box.

upgrade stuff:

* answer question to libc6 "Restart services automaticaly" with "yes"
* say no to other other questions.

commands:

	sed -i "s#jessie#stretch#g" /etc/apt/sources.list.d/debian.list /etc/apt/sources.list.d/security.list &&
	dpkg --clear-avail &&
	if [ -e /etc/apt/sources.list.d/buildd.debian.org.list ]; then
		sed -i "s#jessie#stretch#g" /etc/apt/sources.list.d/buildd.debian.org.list
	fi &&
	apt-get update &&
	service samhain stop &&
	rm -f /var/state/samhain/samhain_file /var/lib/samhain/samhain_file &&
	apt-get install dpkg apt samhain linux-image-amd64 &&
	service samhain stop &&
	apt-get dist-upgrade &&
	rm -f /var/state/samhain/samhain_file /var/lib/samhain/samhain_file &&
	samhain --foreground -t init -p none -s none -l none -m none &&
	(puppet agent -t || true) &&
	: sometimes puppet resets our sources back to jessie.  insist &&
	sed -i "s#jessie#stretch#g" /etc/apt/sources.list.d/debian.list /etc/apt/sources.list.d/security.list &&
	apt-get update &&
	apt-get dist-upgrade &&
	: Only try to purge packages that are installed, else apt gets angry &&
	apt-get purge $(for i in libssl1.0.0:amd64 gcc-4.9-base:amd64 gcc-4.8-base:amd64 libapt-pkg4.12:amd64 libreadline6:amd64  libprocps3:amd64 sp libsp1c2 libaugeas-ruby1.8 libreadline6:arm64 gcc-4.9-base:arm64 gcc-4.8-base:arm64 libruby2.1:arm64 ruby2.1 libssl1.0.0:arm64 libaugeas-ruby sysvinit libxtables10; do dpkg -l "$i" 2>/dev/null | grep -q '^ii' && echo "$i"; done) &&
	(puppet agent -t || true) &&
	apt-get --purge autoremove &&
	while [ "$(deborphan -n | wc -l)" -gt 0 ] ; do apt-get purge $(deborphan -n); done &&
	dpkg --clear-avail &&
	apt-get clean

on amd64, install kernel:

	apt-get install linux-image-amd64

purge removed packages

	dpkg --get-selections | awk '$2=="deinstall" {print $1}' &&
	echo "really purge these [y/N]?" && read ans && [ "$ans" = "y" ] && dpkg --purge `dpkg --get-selections | awk '$2=="deinstall" {print $1}'` &&
	echo "These are not at install:" && dpkg --get-selections | awk '$2!="install" {print $1}'

more clean ups:

	apt-get update &&
	/usr/lib/nagios/plugins/dsa-check-packages | tr -d ,

	(apt-get purge them)

	apt-get --purge autoremove &&
	while [ "$(deborphan -n | wc -l)" -gt 0 ] ; do apt-get purge $(deborphan -n); done

	(puppet agent -t || true) && (puppet agent -t || true)

On HP servers, add nopat to GRUB_CMDLINE_LINUX in /etc/default/grub. ** This should now be done by puppet.  If not, check why not.  (in grub.d)

On ppc64el (only VMs?) the network interface has been renamed from ethX to ibmvethX. Change /etc/network/interfaces accordingly.

update dsa-nagios.git (add host to stretch hostgroup)

re-init samhain and finish with a reboot

	(puppet agent -t || true) &&
	(puppet agent -t || true) &&
	(puppet agent -t || true) &&
	samhain --foreground -t update -p none -s none -l none -m none &&
	dsa-update-apt-status -f &&
	reboot < /dev/null

consider more cleanup

	apt-get autoremove &&
	(puppet agent -t || true) &&
	samhain --foreground -t update -p none -s none -l none -m none &&
	dsa-update-apt-status -f

