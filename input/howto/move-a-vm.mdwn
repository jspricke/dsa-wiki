# Move a VM from one cluster to another

* set `dnsTTL` to 30 in ldap for adayevskaya
* on the target create a new LV to accept the instance
    ```
    lvcreate -n install-adayevskaya -L 5g vg_ganeti
    ```
* on the source, shut down the instance
    ```
    gnt-instance shutdown adayevskaya.debian.org·
    ```
* give the source host ssh access to the target:
  * `cat /etc/ssh/ssh_host_rsa_key.pub`
  * `tee -a /etc/ssh/userkeys/root`
  * `iptables -I INPUT -s bm-bl1 -j ACCEPT`
  * `ip6tables -I INPUT -s bm-bl1 -j ACCEPT`
* copy it over:
    ```
    cat /dev/mapper/adayevskaya | ssh root@manda-node03 'cat > /dev/vg_ganeti/install-adayevskaya'
    # pidof cat, and cat /proc/<pid>/fdinfo/[01]  to check on progress
    ```
* clean up ssh and the firewall again:
    ```
    puppet agent -t
    ferm -i /etc/ferm/ferm.conf
    ```
* resize partitions if the new block device is larger using `fdisk`.  Delete and create the partition at the same starting position.
* open the partition table:
    ```
    kpartx -a -v -p -part /dev/vg_ganeti/install-adayevskaya
    ```
* fsck the filesystem if required, resize if necessary
    ```
    fsck.ext4 /dev/mapper/vg_ganeti-install--adayevskaya-part1
    resize2fs /dev/mapper/vg_ganeti-install--adayevskaya-part1
    ```
* mount the partition and fix etc/network/interfaces:
    ```
    mkdir /mnt/target-adayevskaya
    mount /dev/mapper/vg_ganeti-install--adayevskaya-part1 /mnt/target-adayevskaya
    cd /mnt/target-adayevskaya
    vi etc/hosts etc/network/interfaces
    cd /
    umount /mnt/target-adayevskaya
    rmdir /mnt/target-adayevskaya
    ```
* remove partitions
    ```
    kpartx -d -v -p -part /dev/vg_ganeti/install-adayevskaya
    ```
* create the instance, adopting the block device
    ```
    gnt-instance add \
      --no-ip-check \
      --no-name-check \
      -t blockdev \
      --no-start \
      -t plain \
      --disk=0:adopt=install-adayevskaya \
      --backend-parameters memory=2g,vcpus=2 \
      --os-type noop \
      -n manda-node03 \
      adayevskaya.debian.org

    gnt-instance modify \
      --disk-template drbd \
      -n manda-node04.debian.org \
      adayevskaya.debian.org
    ```
* start the instance, check everything works
    ```
    gnt-instance start adayevskaya.debian.org
    gnt-instance console adayevskaya.debian.org
    ```
* update ud-ldap (ip addresses, physicalHost, sponsor, location, dnsTTL) and ud-generate
* ud-replicate on denis
* check puppet and ssh authkeys on all hosts for the old IP address
* update nagios
* update dsa-passwords
* update reverse DNS
* zero volumes on the old cluster
* remove the instance on the old cluster
* remove volumes from puppet/multipath/storage arrays and our google docs LUN map
