Some things DSA members might like to do:

 * setup procmail to discard mail about successful backups
   and mail about commits to the staging branch of dsa-puppet
 * add an account on nagios.d.o htpasswd
 * add nagios account to nagios contacts
 * run nagstamon or load nagios.d.o daily
 * load munin.d.o/problems.html daily
 * always update samhain after doing anything
 * when adding new nagios checks, don't enable them on all hosts until they have propagated
 * setup puppet pre-commit lint check:

        sudo apt install puppet puppet-lint puppet-strings r10k
        git clone https://github.com/cmeissner/puppet-git-hooks /path/to/puppet-git-hooks
        cat >> /path/to/puppet-git-hooks/commit_hooks/config.cfg << EOF
        # local changes
        CHECK_PUPPET_LINT="permissive" # enabled, permissive or disabled (permissive runs but return code is ignored)
        CHECK_PUPPET_DOCS="permissive" # enabled, permissive or disabled (permissive runs but return code is ignored)
        EOF
        ln -s /absolute/path/to/puppet-git-hooks/pre-commit /path/to/dsa-puppet/.git/pre-commit
