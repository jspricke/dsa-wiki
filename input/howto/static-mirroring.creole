= static mirroring

The static mirroring system mirrors //components//, i.e. directory trees from
their individual //source// hosts via //static-master// hosts to the
//static-mirrors//.

The static-master can be configured per source.  The mirrors are the same set
for all components.

== Adding a new static component

Using already configured source and master hosts.

* Pick a component name, e.g., {{{debaday.debian.net}}}

* Pick/Learn source host, pick a master, and add to {{{dsa-puppet/modules/roles/misc/static-components.yaml}}}:
{{{
       debaday.debian.net:
         master: static-master-manda-01.debian.org
         source: dillon.debian.org:/srv/debaday.debian.net/htdocs
}}}

* Give sudo to run the right {{{static-update-component}}} to the appropriate group:
{{{
	/usr/sbin/visudo -f dsa-puppet/modules/sudo/files/sudoers
}}}
and add something like
{{{
	lucas           dillon=(staticsync)             NOPASSWD: /usr/local/bin/static-update-component debaday.debian.net
}}}

* Add the vhost config on mirrors:
** vi {{{dsa-puppet/modules/roles/templates/static-mirroring/vhost/static-vhosts-simple.erb}}}
and add
<pre>
  vhost(lines, "debaday.debian.net"            , :ssl => true
</pre>
and
<pre>
  onion::service { 'debaday.debian.net'            : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
</pre>
** In {{{dsa-puppet/modules/roles/manifests/static_mirror_web.pp}}}, add:
{{{
	ssl::service { 'debaday.debian.net'            : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
}}}

* run puppet on the master, the source, and all the mirrors.  Check {{{dsa-puppet/data/nodes/*.yaml}}}.

== Adding a mirror

Note:  This might temporarily break apache on the new mirror host.

* Add {{{roles::static_mirror}}} to the {{{classes}}} stanza in {{{data/nodes/$HOSTNAME.yaml}}}.
* Add {{{staticsync::static_mirror::get_triggered: false}}} to {{{data/nodes/$HOSTNAME.yaml}}}.
* Add {{{staticsync}}} group to host's {{{allowedGroups}}} attribute in ldap.
* Run puppet twice on new static mirror once staticsync exists.
* Run puppet on static masters.
* On the static mirror: {{{install -d -o staticsync -g staticsync -m 02755 /srv/static.debian.org}}}.
* Run the command from {{{/etc/cron.d/puppet-static-mirror}}} on the mirror.
* Make sure apache works on the mirror.  This might require deploying SSL keys.
* Remove {{{staticsync::static_mirror::get_triggered: false}}} from {{{data/nodes/$HOSTNAME.yaml}}}.
* Run puppet on static masters.
* Rotate mirror.
