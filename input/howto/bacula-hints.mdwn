## useful bacula commands 

### find out what was in a backup run
 * get the job-id from the bacula mail
 * run the following on bacula database (currently bmdb1.debian.org)
```
select path,name from filename inner join file on filename.filenameid=file.filenameid inner join path on path.pathid=file.pathid where file.jobid=$JOBID;
```
### reschedule a backup run
 * check the debian-admin-bacula archive on master for failed runs
 * if the issue is intermittent or fixed, log in to dinis
 * run bconsole
 * type "run", and pick the host
 * if necessary adjust "level", then start the backup

### do a mass reschedule on what is needed
```
(cd /etc/bacula/conf.d && for i in *; do h="${i%%.conf}"; /usr/lib/nagios/plugins/dsa-check-bacula -w 17h $h > /dev/null|| echo "run $h yes"; done) |tee incruns
```
