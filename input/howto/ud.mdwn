# how to submit changes to ud via mail

## example

Create a text file containing the changes:

      username@hostname$ cat changes.txt
      update c us
      username@hostname$ 

Email the file, gpg signed and armored, to ud@db.debian.org:

      username@hostname$ cat changes.txt | gpg --sign --armor | mail -s 'changes' ud@db.debian.org

## syntax

The general syntax of commands is:

      update <key> <val>
      delete <key> [<val>]
      show

where the available keys are:

      key: bATVToken
      val: text (eg: mytoken)
      
      key: birthDate
      val: yyyymmdd (eg: 19700101)
      
      key: c
      val: two-character country code (eg: CA)
      
      key: dnsZoneEntry
      val:
      
      key: emailForward
      val: email address (eg: luca.filipozzi@example.com)
      
      key: facsimileTelephoneNumber
      val: telephone number in international representation (eg: +1 604 867 5309)
      
      key: gender
      val: one of 0, 1, 2, 9 for unknown, male, female, unspecified
      
      key: icqUin
      val: 
      
      key: ircNick
      val: as registered at irc.debian.org
      
      key: jabberJID
      val: fully qualified jid (eg: foo@jabber.org)
      
      key: l
      val: locality name (eg: Vancouver BC)
      
      key: labeledURI
      val:
      
      key: latitude
      val: ±DDDMMSS
      
      key: longitude
      val: ±DDDMMSS
      
      key: mailCallout
      val:
      
      key: mailContentInspectionAction
      val:
      
      key: mailDefaultOptions
      val:
      
      key: mailGreylisting
      val:
      
      key: mailRBL
      val:
      
      key: mailRHSBL
      val:
      
      key: mailWhitelist
      val:
      
      key: sshRSAAuthKey
      val:

