= how to add a new machine =

Note: this has recently been changed to rely more on [[puppet|howto/puppet-setup]].  If stuff breaks fix it.


* some initial stuff:
{{{
    apt-get install --no-install-recommends ssh vim &&
    apt-get install --no-install-recommends dialog &&
    echo "debconf debconf/priority        select high" | debconf-set-selections &&
    echo "debconf debconf/frontend        select Dialog" | debconf-set-selections
}}}

* unless we want to keep it:
{{{
    dpkg -l postfix | grep '^ii  postfix' && (dpkg --purge postfix && rm /etc/aliases)
}}}

* on draghi, add the host to the ldap using ud-host.  Set the ssh key and the IP Address attributes.

* run generate, or wait until cron runs it for you.  Update DNS.
{{{
    : :: draghi :: && sudo -u sshdist ud-generate && sudo -H ud-replicate && sudo -H puppet agent -t
    : :: denis :: && sudo -H ud-replicate
}}}

* setup [[puppet|howto/puppet-setup]]  (run the puppet client two or three times until things converge.)

* on the host, run ud-replicate and check if it worked
{{{
    apt-get update;
    puppet agent -t; puppet agent -t; puppet agent -t
    ud-replicate
    puppet agent -t; puppet agent -t; puppet agent -t
}}}

* install security updates etc.
{{{
    apt-get update && apt-get dist-upgrade && apt-get clean
}}}

* install samhain and get puppet to configure it
{{{
  apt-get install -y samhain &&
  ( puppet agent -t || true ) &&
  service samhain stop &&
  rm -f /var/state/samhain/samhain_file /var/lib/samhain/samhain_file &&
  samhain --foreground -t init -p none -s none -l none -m none &&
  service samhain start
}}}

* if it is a HP Proliant, or has other management fu, read [[howto/ilo-https]]

* edit dedication into in $DSA-PUPPET/modules/debian_org/files/misc/local.yaml

* add to nagios

-- weasel, Wed, 04 Jun 2008 20:52:56 +0200
